//= ../../node_modules/jquery/dist/jquery.js
//= ../../node_modules/swiper/dist/js/swiper.jquery.js

$(document).ready(function () {
    $(".switch").on('click', function () {
        var img = $(this).find('img').attr('src');
        var name = $(this).find('.name').text();
        var info = $(this).find('.info').text();
        $('.main_photocard img').attr('src', img);
        $('.main_photocard .name').text(name);
        $('.main_photocard .info').text(info);
    });

});


var swiper = new Swiper('.swiper-container', {
    slidesPerView: 3,
    paginationClickable: true,
    spaceBetween: 30,
    loop: true,
    breakpoints: {
        480: {
            slidesPerView: 1
        },
        992: {
            slidesPerView: 2
        }
    }
});

var map;
function initMap() {
    var myLatLng = {lat: 50.3954337, lng: 30.6338324};

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 50.3954337, lng: 30.6338324},
        zoom: 16,
        scrollwheel: false
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map
    });
}
